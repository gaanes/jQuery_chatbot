<!DOCTYPE html>
<html>
<head>
	<title>JQuery Chatbox</title>
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<div id="header">
	
		<h1>JQuery Chatbot v. 1.0</h1>
	
</div>
<div id="container"></div>
<div id="controls">
	<textarea id="textbox" placeholder="Enter your Message here..."></textarea>
	<button id="send">Send</button>
	<br>
	<input checked type="checkbox" id="enter" >
	<label>Send on Enter</label>
</div>
</body>
</html>